var metaKeys;

metaKeys = (function() {
  function metaKeys(ctrl, alt, meta) {
    this.ctrl(ctrl);
    this.alt(alt);
    this.meta(meta);
  }

  metaKeys.prototype.setCtrl = function(state) {
    if (state) {
      this.ctrl = true;
    } else {
      this.ctrl = false;
    }
    return this;
  };

  metaKeys.prototype.setAlt = function(state) {
    if (state) {
      this.alt = true;
    } else {
      this.alt = false;
    }
    return this;
  };

  metaKeys.prototype.setMeta = function(state) {
    if (state) {
      this.meta = true;
    } else {
      this.meta = false;
    }
    return this;
  };

  return metaKeys;

})();
