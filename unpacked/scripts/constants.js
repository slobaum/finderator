var constants, namespace;

namespace = 'finderator';

constants = {
  classes: {
    inputVisible: "" + namespace + "-show"
  },
  key: {
    space: 32,
    f: 70,
    down: 40,
    up: 38,
    enter: 13,
    escape: 27,
    tab: 9
  }
};
