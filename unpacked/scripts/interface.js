var shortcut;

$(function() {
  return shortcut.init();
});

shortcut = {
  init: function() {
    this.window = $(window);
    this.output = $('#current-shortcut').on('click', this.capture.bind(this));
    this.warnings = {
      'noActivation': $('#no-activation-warning'),
      'noModifier': $('#no-modifier-warning'),
      'invalidCombo': $('#invalid-combo-warning')
    };
    this.modifierKeys = $('.modifier-key').on('change', this.checkWarnings.bind(this));
    return this.checkWarnings();
  },
  capture: function() {
    this.window.on('keypress.shortcut', this.keypress.bind(this));
    this.window.on('keydown.shortcut', this.keydown.bind(this));
    return this.output.text('Press key...').addClass('setting');
  },
  keydown: function(event) {
    var _ref;
    if ((_ref = event.keyCode) === 27 || _ref === 8 || _ref === 13) {
      this.setCharCode(null, 'Not set');
      return event.preventDefault();
    }
  },
  keypress: function(event) {
    var character, code;
    code = event.charCode;
    character = String.fromCharCode(code);
    if (code === 32) {
      character = 'Space';
    }
    return this.setCharCode(code, character);
  },
  setCharCode: function(code, output) {
    this.window.off('.shortcut');
    this.key = code;
    this.output.text(output).removeClass('setting');
    return this.checkWarnings();
  },
  checkWarnings: function() {
    if (!this.key) {
      this.warnings.noActivation.removeClass('hide');
    } else {
      this.warnings.noActivation.addClass('hide');
    }
    if (!this.modifierKeys.filter(':checked').length) {
      return this.warnings.noModifier.removeClass('hide');
    } else {
      return this.warnings.noModifier.addClass('hide');
    }
  }
};
