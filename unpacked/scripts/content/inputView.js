var inputView;

inputView = {
  generateTemplate: function() {
    return $(("<div class='" + namespace + "-container'> <input type='text' class='" + namespace + "-input' tab-index='-1'>") + ("<div class='" + namespace + "-info'></div> </div>"));
  },
  init: function($target) {
    this.element = this.generateTemplate().prependTo($target);
    this.input = this.element.find("." + namespace + "-input").on('keyup', this.keyup.bind(this)).on('keydown', this.keydown.bind(this));
    return this.info = this.element.find("." + namespace + "-info");
  },
  keydown: function(event) {
    switch (event.keyCode) {
      case constants.key.tab:
        if (event.shiftKey) {
          return this.prevResult(event);
        } else {
          return this.nextResult(event);
        }
        break;
      case constants.key.down:
        return this.nextResult(event);
      case constants.key.up:
        return this.prevResult(event);
      case 13:
        return resultsView.trigger('mousedown');
    }
  },
  keyup: function(event) {
    var value, _ref;
    if ((_ref = event.keyCode) === constants.key.down || _ref === constants.key.up) {
      return;
    }
    if (event.keyCode === constants.key.escape) {
      return this.hide();
    }
    if (event.keyCode === constants.key.enter) {
      return resultsView.trigger('mouseup', 'click');
    }
    value = this.input.val();
    if (value.length > 1) {
      if (resultsView.currentTerm !== value) {
        search.withTerm(value);
        resultsView.currentTerm = value;
      }
    } else {
      resultsView.clear();
    }
    return this.updateInfo();
  },
  isVisible: function() {
    return this.element.hasClass(constants.classes.inputVisible);
  },
  toggle: function() {
    if (this.isVisible()) {
      return this.hide();
    } else {
      return this.show();
    }
  },
  show: function() {
    var input;
    this.element.addClass(constants.classes.inputVisible);
    input = this.input.get(0);
    input.focus();
    return input.select();
  },
  hide: function() {
    this.element.removeClass(constants.classes.inputVisible);
    this.input.get(0).blur();
    return resultsView.clear();
  },
  updateInfo: function() {
    return this.info.text(resultsView.getInfo());
  },
  nextResult: function(event) {
    event.preventDefault();
    resultsView.nextIndex();
    return this.updateInfo();
  },
  prevResult: function(event) {
    event.preventDefault();
    resultsView.prevIndex();
    return this.updateInfo();
  }
};
