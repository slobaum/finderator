var controller;

controller = {
  init: function(target) {
    this.target = $(target);
    inputView.init(this.target);
    return $(window).on('keydown', this.keydown.bind(this));
  },
  keyActivated: function(event) {
    return event.ctrlKey && event.keyCode === constants.key.f;
  },
  keydown: function(event) {
    if (this.keyActivated(event)) {
      event.preventDefault();
      return inputView.toggle();
    }
  }
};

$(function() {
  return controller.init(document.body);
});
