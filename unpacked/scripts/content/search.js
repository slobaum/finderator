var search;

search = {
  treeWalk: function(lambda, node) {
    var startNode;
    if (!node) {
      node = document.body;
    }
    startNode = node;
    while (node) {
      if (node.firstChild) {
        node = node.firstChild;
      } else if (node.nextSibling) {
        node = node.nextSibling;
      } else {
        while (!node.nextSibling) {
          node = node.parentNode;
          if (node === startNode) {
            return;
          }
        }
        node = node.nextSibling;
      }
      lambda(node);
    }
  },
  withTerm: function(term) {
    var regexp;
    term = term.replace(/([.*+?^${}()|[\]\/\\])/g, '\\$1');
    resultsView.clear();
    if (!term) {
      return;
    }
    regexp = new RegExp(term, 'i');
    this.treeWalk((function(_this) {
      return function(node) {
        var parentNodeName;
        parentNodeName = node.parentNode.nodeName.toLowerCase();
        if (node.nodeType === 3 && !node.parentNode.getAttribute("data-" + namespace) && parentNodeName !== 'script' && parentNodeName !== 'style') {
          while (node.nextSibling && node.nextSibling.nodeType === 3) {
            node.nodeValue += node.nextSibling.nodeValue;
            node.parentNode.removeChild(node.nextSibling);
          }
          return _this.processNode(node, regexp);
        }
      };
    })(this));

    /*
    @walker = document.createTreeWalker(
        document.body,
        NodeFilter.SHOW_TEXT,
            acceptNode: (node)->
                while node.nextSibling && node.nextSibling.nodeType == 3
                    node.nodeValue += node.nextSibling.nodeValue
                    node.parentNode.removeChild(node.nextSibling)
                !node.parentNode.getAttribute("data-#{namespace}") && !$(node).closest('script').length && $(node.parentNode).is(':visible')
    )
    while @walker.nextNode()
        @processNode @walker.currentNode, regexp
     */
    return null;
  },
  processNode: function(node, regexp) {
    var afterMatchText, index, matchText, originalValue;
    originalValue = node.nodeValue;
    index = originalValue.search(regexp);
    if (index > -1) {
      node.nodeValue = originalValue.substring(0, index);
      matchText = originalValue.match(regexp)[0];
      afterMatchText = originalValue.substring(index + matchText.length, originalValue.length);
      node.parentNode.insertBefore(document.createTextNode(afterMatchText), node.nextSibling);
      return resultsView.generateHighlight(matchText).insertAfter(node);
    }
  }
};
