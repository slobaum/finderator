var resultsView,
  __slice = [].slice;

resultsView = {
  generateHighlight: function(text) {
    var highlight;
    highlight = $("<ins class='" + namespace + "-search-result' data-" + namespace + "='true'>" + text + "</ins>");
    this.cache = this.cache.add(highlight);
    setTimeout(((function(_this) {
      return function() {
        if (_this.selectedIndex == null) {
          return _this.highlightIndex(0);
        }
      };
    })(this)), 0);
    return highlight;
  },
  clear: function() {
    if (this.cache) {
      this.cache.each(function(i, element) {
        return $(element).before(element.firstChild).remove();
      });
    }
    this.cache = $([]);
    this.selectedIndex = null;
    return this.currentTerm = null;
  },
  highlightIndex: function(index) {
    if (index < 0) {
      index = this.cache.length + index;
    } else if (index >= this.cache.length) {
      index = 0;
    }
    this.cache.each(function(thisIndex, element) {
      return $(element)[index == thisIndex ? 'addClass' : 'removeClass'](namespace + "-selected");
    });
    this.selectedIndex = index;
    return this.scrollIntoView();
  },
  nextIndex: function() {
    if (this.selectedIndex != null) {
      ++this.selectedIndex;
    } else {
      this.selectedIndex = 0;
    }
    return this.highlightIndex(this.selectedIndex);
  },
  prevIndex: function() {
    if (this.selectedIndex != null) {
      --this.selectedIndex;
    } else {
      this.selectedIndex = -1;
    }
    return this.highlightIndex(this.selectedIndex);
  },
  scrollIntoView: function() {
    var bounds, padding, scrollTop, target, viewport, win;
    if (this.selectedIndex != null) {
      target = $(this.cache.get(this.selectedIndex));
      win = $(window);
      viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft(),
        height: win.height()
      };
      bounds = target.offset();
      viewport.bottom = viewport.top + viewport.height;
      bounds.bottom = bounds.top + target.outerHeight();
      padding = 20;
      if (viewport.top > bounds.bottom - padding) {
        scrollTop = bounds.top - padding;
      } else if (viewport.bottom < bounds.top + padding) {
        scrollTop = (bounds.bottom - viewport.height) + padding;
      }
      return (this.scroller || (this.scroller = $('html, body'))).stop().animate({
        scrollTop: scrollTop
      }, 'fast');
    }
  },
  trigger: function() {
    var event, events, target, _i, _len, _results;
    events = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    target = $(this.cache.get(this.selectedIndex));
    _results = [];
    for (_i = 0, _len = events.length; _i < _len; _i++) {
      event = events[_i];
      _results.push(target.trigger(event));
    }
    return _results;
  },
  getInfo: function() {
    if (this.cache.length) {
      return "" + (this.selectedIndex + 1) + " of " + this.cache.length;
    } else if (this.currentTerm) {
      return "No results";
    } else {
      return '';
    }
  }
};
