var repo;

repo = {
  store: function(key, value) {
    if (value != null) {
      return window.localStorage[key] = value;
    } else {
      return window.localStorage[key];
    }
  },
  getMetaKeys: function() {
    return this.store('key-meta');
  },
  setMetaKeys: function(meta) {
    if (!(meta instanceof metaKeys)) {
      throw new Error('setMetaKeys expects an instance of metaKeys');
    }
    this.store('key-meta', meta);
    return this;
  },
  getKey: function() {
    return this.store('key');
  },
  setKey: function(charCode) {
    this.store('key', charCode);
    return this;
  }
};
