module.exports = (grunt)->
    grunt.loadNpmTasks 'grunt-contrib-less'
    grunt.loadNpmTasks 'grunt-contrib-coffee'
    # grunt.loadNpmTasks 'grunt-chrome-compile'
    grunt.loadNpmTasks 'grunt-crx'

    grunt.initConfig(
        pkg: grunt.file.readJSON 'package.json'
        less:
            development:
                options:
                    compress: true
                    yuicompress: true
                    optimization: 2
                files:
                    ## target.css file: source.less file
                    'unpacked/styles/content.css': 'source/styles/content.less'
                    'unpacked/styles/interface.css': 'source/styles/interface.less'
        coffee:
            compile:
                expand: true
                options:
                    bare: true
                cwd: 'source/scripts/'
                src: ['**/*.coffee']
                dest: 'unpacked/scripts/'
                ext: '.js'
        
        crx:
            finderator:
                'src': 'unpacked/'
                'dest': 'dist/<%= pkg.name %>-<%= pkg.version %>.crx'
                'privateKey': 'cert/cert.pem'
        
## FOR package.json    "grunt-chrome-compile": "~0.2.2",
#        'chrome-extension':
#            options:
#                name: '<%= pkg.name %>'
#                version: '<%= pkg.version %>'
#                # id: '00000000000000000000000000000000'
#                buildDir: 'dist/'
#                # certDir: 'dist/'
#                resources: [ 'unpacked/*' ]
    )

    grunt.registerTask 'default', [
        'less'
        'coffee'
        # 'crx'
    ]
