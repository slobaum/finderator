$ ->
  shortcut.init()

shortcut =
  init: ->
    @window = $(window)
    @output = $('#current-shortcut').on 'click', @capture.bind @
    @warnings =
      'noActivation': $ '#no-activation-warning'
      'noModifier': $ '#no-modifier-warning'
      'invalidCombo': $ '#invalid-combo-warning'
    @modifierKeys = $('.modifier-key').on 'change', @checkWarnings.bind @
    @checkWarnings()
  capture: ->
    @window.on 'keypress.shortcut', @keypress.bind @
    @window.on 'keydown.shortcut', @keydown.bind @
    @output
      .text 'Press key...'
      .addClass 'setting'
  keydown: (event)->
    if event.keyCode in [ 27, 8, 13 ]
      @setCharCode null, 'Not set'
      event.preventDefault()
  keypress: (event)->
    code = event.charCode
    character = String.fromCharCode(code)
    if code is 32 then character = 'Space'
    @setCharCode code, character
  setCharCode: (code, output)->
    @window.off '.shortcut'
    @key = code
    @output
      .text output
      .removeClass 'setting'
    @checkWarnings()
  checkWarnings: ->
    ## also add check for invalid combinations (CTRL+N)

    if !@key
      @warnings.noActivation.removeClass 'hide'
    else
      @warnings.noActivation.addClass 'hide'

    if !@modifierKeys.filter(':checked').length
      @warnings.noModifier.removeClass 'hide'
    else
      @warnings.noModifier.addClass 'hide'