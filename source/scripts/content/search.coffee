search =
    treeWalk: (lambda, node)->
        if (!node)
            node = document.body;
        startNode = node;

        while node
            if node.firstChild
                node = node.firstChild
            else if node.nextSibling
                node = node.nextSibling
            else
                while (!node.nextSibling)
                    node = node.parentNode
                    if (node == startNode)
                        return
                node = node.nextSibling
            lambda node
    withTerm: (term)->
        term = term.replace(/([.*+?^${}()|[\]\/\\])/g, '\\$1')
        resultsView.clear()
        return if !term
        regexp = new RegExp term, 'i'

        @treeWalk((node)=>
            parentNodeName = node.parentNode.nodeName.toLowerCase()
            if node.nodeType == 3 and !node.parentNode.getAttribute("data-" + namespace) and parentNodeName != 'script' and parentNodeName != 'style'
                while node.nextSibling and node.nextSibling.nodeType == 3
                    node.nodeValue += node.nextSibling.nodeValue;
                    node.parentNode.removeChild(node.nextSibling);
                @processNode node, regexp;
        );

        ###
        @walker = document.createTreeWalker(
            document.body,
            NodeFilter.SHOW_TEXT,
                acceptNode: (node)->
                    while node.nextSibling && node.nextSibling.nodeType == 3
                        node.nodeValue += node.nextSibling.nodeValue
                        node.parentNode.removeChild(node.nextSibling)
                    !node.parentNode.getAttribute("data-#{namespace}") && !$(node).closest('script').length && $(node.parentNode).is(':visible')
        )
        while @walker.nextNode()
            @processNode @walker.currentNode, regexp
        ###
        null
    processNode: (node, regexp)->
        originalValue = node.nodeValue
        index = originalValue.search(regexp);
        if index > -1
            node.nodeValue = originalValue.substring 0, index
            matchText = originalValue.match(regexp)[0]
            afterMatchText = originalValue.substring index + matchText.length, originalValue.length
            node.parentNode.insertBefore(
                document.createTextNode(afterMatchText),
                node.nextSibling
            )
            resultsView.generateHighlight(matchText).insertAfter node