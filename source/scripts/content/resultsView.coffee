resultsView =
    generateHighlight: (text)->
        highlight = $ "<ins class='#{namespace}-search-result' data-#{namespace}='true'>#{text}</ins>"
        @cache = @cache.add highlight
        # if @cache.length == 1 # is first highlight?
        #    @highlightIndex 0
        ## // HAS UNINTENDED SIDE EFFECT. when first one is selected, it is not yet on the DOM at this point, so scrolls to top of window
        setTimeout (=>
            if !@selectedIndex?
                @highlightIndex 0
        ), 0
        highlight
    clear: ->
        if @cache
            @cache.each (i, element)->
                $(element).before(element.firstChild).remove()
        @cache = $([])
        @selectedIndex = null
        @currentTerm = null
    highlightIndex: (index)->
        if index < 0
            index = @cache.length + index
        else if index >= @cache.length
            index = 0
        @cache.each((thisIndex, element)->
            ## wtf, coffescript mangles this statement?
            ## of course, no ternary operator :P
            `$(element)[index == thisIndex ? 'addClass' : 'removeClass'](namespace + "-selected")`
        )
        @selectedIndex = index
        @scrollIntoView()
    nextIndex: ->
        if @selectedIndex?
            ++@selectedIndex
        else
            @selectedIndex = 0
        @highlightIndex @selectedIndex
    prevIndex: ->
        if @selectedIndex?
            --@selectedIndex
        else
            @selectedIndex = -1
        @highlightIndex @selectedIndex
    scrollIntoView: ->
        if @selectedIndex?
            target = $(@cache.get(@selectedIndex))
            win = $ window;
            viewport =
                top: win.scrollTop()
                left: win.scrollLeft()
                height: win.height()
            bounds = target.offset()

            #viewport.right = viewport.left + win.width();
            viewport.bottom = viewport.top + viewport.height;
    
            #bounds.right = bounds.left + this.outerWidth();
            bounds.bottom = bounds.top + target.outerHeight();
            
            padding = 20
            if viewport.top > bounds.bottom - padding
                scrollTop = bounds.top - padding
            else if viewport.bottom < bounds.top + padding
                scrollTop = (bounds.bottom - viewport.height) + padding

            (@scroller || @scroller = $('html, body')).stop().animate({ scrollTop: scrollTop }, 'fast')
    trigger: (events...)->
        target = $(@cache.get(@selectedIndex))
        for event in events
            target.trigger(event)
    getInfo: ->
        if @cache.length
            "#{@selectedIndex + 1} of #{@cache.length}"
        else if @currentTerm
            "No results"
        else ''