controller = 
    init: (target)->
        @target = $(target)
        inputView.init(@target);
        $(window)
            .on 'keydown', @keydown.bind @
    keyActivated: (event) ->
        event.ctrlKey and event.keyCode is constants.key.f
    keydown: (event) ->
        if @keyActivated(event)
            event.preventDefault()
            inputView.toggle()

$ ->
    controller.init document.body