inputView =
    generateTemplate: ->
        $ "<div class='#{namespace}-container'>
                 <input type='text' class='#{namespace}-input' tab-index='-1'>" + 
                 "<div class='#{namespace}-info'></div>
             </div>"
    init: ($target)->
        @element = this.generateTemplate().prependTo $target
        @input = @element.find ".#{namespace}-input"
            .on 'keyup', @keyup.bind @
            .on 'keydown', @keydown.bind @
        @info = @element.find ".#{namespace}-info"
    keydown: (event) ->
        switch event.keyCode
            when constants.key.tab
                if event.shiftKey
                    @prevResult event
                else
                    @nextResult event
            when constants.key.down
                @nextResult event
            when constants.key.up
                @prevResult event
            when 13 then resultsView.trigger 'mousedown'
    keyup: (event) ->
        if event.keyCode in [constants.key.down, constants.key.up]
            return
        if event.keyCode == constants.key.escape
            return @hide()
        if event.keyCode == constants.key.enter
            return resultsView.trigger 'mouseup', 'click'
        value = @input.val()
        if value.length > 1
            if resultsView.currentTerm != value
                search.withTerm value
                resultsView.currentTerm = value
        else
            resultsView.clear()
        @updateInfo()
    isVisible: ->
        @element.hasClass constants.classes.inputVisible
    toggle: ->
        if @isVisible()
            @hide()
        else
            @show()
    show: ->
        @element.addClass constants.classes.inputVisible
        input = @input.get(0)
        input.focus()
        input.select()
    hide: ->
        @element.removeClass constants.classes.inputVisible
        @input.get(0).blur()
        resultsView.clear()
    updateInfo: ->
        @info.text resultsView.getInfo()
    nextResult: (event)->
        event.preventDefault()
        resultsView.nextIndex()
        @updateInfo()
    prevResult: (event)->
        event.preventDefault()
        resultsView.prevIndex()
        @updateInfo()