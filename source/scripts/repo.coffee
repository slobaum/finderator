repo =
	store: (key, value)->
		if value?
			window.localStorage[key] = value
		else
			return window.localStorage[key];

	getMetaKeys: ->
		return @store 'key-meta'
	setMetaKeys: (meta)->
		if !(meta instanceof metaKeys)
			throw new Error 'setMetaKeys expects an instance of metaKeys'
		@store 'key-meta', meta
		@

	getKey: ->
		return @store 'key'
	setKey: (charCode)->
		@store 'key', charCode
		@