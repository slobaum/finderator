    _______ _____ __   _ ______  _______  ______ _______ _______  _____   ______
    |______   |   | \  | |     \ |______ |_____/ |_____|    |    |     | |_____/
    |       __|__ |  \_| |_____/ |______ |    \_ |     |    |    |_____| |    \_

v0.1
### By: Daniel Carter

A find utility for Chrome that lets you activate the highlighted search result.

When users press enter, the element that's highlighted is clicked rather than "finding next".

This subtle variation can make a more keyboard-centric and accessible internet experience.
